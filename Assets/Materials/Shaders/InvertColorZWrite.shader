Shader "Myriad/InvertColorZWrite" {
Properties {
_Color ("Main Color", Color) = (1,1,1,1)
  
}

Category {
 Tags { "Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Opaque" }
 	Blend OneMinusDstColor OneMinusSrcColor  

 	Cull Back 
 	Lighting Off 
 	ZWrite On
  
 SubShader {
 	Pass {
 	 ZTest LEqual
 	 Color [_Color]
 	 } 
 }
}
}