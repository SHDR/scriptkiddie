﻿using UnityEngine;
using System.Collections;

public class Packet : MonoBehaviour {

	void Start ()
    {
	
	}

    public float startSpeed;
    public float homingSpeed;
    public float startAngleRange;
    public float startSpeedDecay;
    public float homingSpeedIncrement;

    private Vector3 _direction = Vector3.zero;
    private Vector3 _targetPosition = Vector3.zero;
    private Node _targetNode;

    public void SetTargetNode ( Node targetNode )
    {
        if ( targetNode != null )
        {
            _targetNode = targetNode;
            _targetPosition = targetNode.transform.position + ( Vector3.up * 0.5f );
            _direction = Quaternion.AngleAxis( Random.Range( -startAngleRange , startAngleRange ) , Vector3.up ) * ( _targetPosition - transform.position ).normalized;
            transform.rotation = Quaternion.LookRotation( _direction , Vector3.up );
            startSpeed *= Random.Range( 0.95f , 1.05f );
            homingSpeed *= Random.Range( 0.95f , 1.05f );
        }
    }

	void Update ()
    {
        if ( _direction != Vector3.zero )
        {
            startSpeed *= startSpeedDecay;
            homingSpeed *= homingSpeedIncrement;

            transform.position += ( _direction * startSpeed ) * Time.deltaTime;
            transform.position += ( ( _targetPosition - transform.position ).normalized * homingSpeed ) * Time.deltaTime;

            if ( Vector2.Distance( transform.position , _targetPosition ) < 0.25f )
            {
                Collider[] colliders = Physics.OverlapSphere( transform.position , 0.25f );

                for ( int i = 0 ; i < colliders.Length ; i++ )
                {
                    if ( colliders[ i ].transform == _targetNode.transform )
                    {
                        _targetNode.RoutePacket();
                        Destroy( gameObject );
                    }
                }
            }
        }
	}
}
