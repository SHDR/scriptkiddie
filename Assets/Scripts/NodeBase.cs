﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NodeBase : MonoBehaviour
{
    #region Initialization

    void Start()
    {
        _lineRenderer.enabled = false;
        MakeNetwork();
    }

    #endregion

    #region Make network

    private void MakeBranch ( int depth , Vector3 position , Vector3 direction , Node prevNodeScript )
    {
        GameObject newNode;
        Node newNodeScript;

        Collider[] colliders = Physics.OverlapSphere( position , 1.25f );

        if ( colliders.Length > 0 )
        {
            newNode = colliders[ 0 ].gameObject;
            newNodeScript = newNode.GetComponent<Node>();

        }
        else
        {
            newNode = Instantiate( nodePrefab ) as GameObject;
            newNodeScript = newNode.GetComponent<Node>();

            nodeGO.Add( newNode );
            nodes.Add( newNodeScript );
            newNode.transform.parent = transform;
            newNode.transform.position = position;
        }

        if ( prevNodeScript != null )
        {
            prevNodeScript.AddConnection( newNodeScript );
            newNodeScript.AddConnection( prevNodeScript );

            switch ( prevNodeScript.nodeType )
            {
                case "target" :

                    newNodeScript.SetNodeType( "blackice" );
                    break;

                case "blackice":

                    newNodeScript.SetNodeType( "botnet" );
                    break;

                case "botnet":

                    if ( Random.Range ( 0f , 1f ) > 0.5f )
                        newNodeScript.SetNodeType( "botnet" );
                    else
                    {
                        if ( Random.Range ( 0f , 1f ) > 0.25f )
                            newNodeScript.SetNodeType( "firewall" );
                        else
                            newNodeScript.SetNodeType( "botnet" );
                    }

                    break;

                case "firewall":

                    if ( Random.Range( 0f , 1f ) > 0.5f )
                        newNodeScript.SetNodeType( "router" );
                    else
                        newNodeScript.SetNodeType( "botnet" );

                    break;

                case "router":

                    if ( Random.Range( 0f , 1f ) > 0.5f )
                        newNodeScript.SetNodeType( "firewall" );
                    else
                        newNodeScript.SetNodeType( "botnet" );

                    break;
            }
        }
        else
        {
            newNodeScript.SetNodeType( "router" );
        }

        if ( depth > 0 )
        {
            float angle = 60f * depth;
            float angleIncrement = angle / depth;

            float evenAngle = 0f;
            float oddAngle = 0f;

            for ( int i = 0 ; i < depth ; i++ )
            {
                if ( depth % 2 == 0 )
                {
                    if ( i % 2 == 0 )
                    {
                        evenAngle += angleIncrement;
                        Vector3 dir = Quaternion.AngleAxis( evenAngle , Vector3.up ) * direction;

                        MakeBranch( depth - 1 , position + ( direction * nodeOffset ) , dir , newNodeScript );
                    }
                    else
                    {
                        oddAngle += angleIncrement;
                        Vector3 dir = Quaternion.AngleAxis( -oddAngle , Vector3.up ) * direction;

                        MakeBranch( depth - 1 , position + ( direction * nodeOffset ) , dir , newNodeScript );
                    }
                }
                else
                {
                    if ( i > 0 )
                    {
                        if ( i % 2 == 0 )
                        {
                            evenAngle += angleIncrement;
                            Vector3 dir = Quaternion.AngleAxis( evenAngle , Vector3.up ) * direction;

                            MakeBranch( depth - 1 , position + ( direction * nodeOffset ) , dir , newNodeScript );
                        }
                        else
                        {
                            oddAngle += angleIncrement;
                            Vector3 dir = Quaternion.AngleAxis( -oddAngle , Vector3.up ) * direction;

                            MakeBranch( depth - 1 , position + ( direction * nodeOffset ) , dir , newNodeScript );
                        }
                    }
                    else
                    {
                        MakeBranch( depth - 1 , position + ( direction * nodeOffset ) , direction , newNodeScript );
                    }
                }
            }
        }
    }

	void MakeNetwork () 
    {
        GameObject targetNode = Instantiate( nodePrefab ) as GameObject;
        nodeGO.Add( targetNode );

        targetNode.transform.parent = transform;

        Node targetNodeScript = targetNode.GetComponent<Node>();
        targetNodeScript.SetNodeType( "target" );
        nodes.Add( targetNodeScript );

        //blackice
        MakeBranch( 2 , Vector3.left * 3.75f , Vector3.left , targetNodeScript );
        MakeBranch( 2 , Vector3.right * 3.75f , Vector3.right , targetNodeScript );
        MakeBranch( 2 , Vector3.forward * 3.75f , Vector3.forward , targetNodeScript );
        MakeBranch( 2 , Vector3.back * 3.75f , Vector3.back , targetNodeScript );

        //inner routers
        MakeBranch( 2 , ( Vector3.left + Vector3.forward ).normalized * 6f , ( Vector3.left + Vector3.forward ).normalized , null );
		MakeBranch( 2 , ( Vector3.right + Vector3.forward ).normalized * 6f , ( Vector3.right + Vector3.forward ).normalized , null );
		MakeBranch( 2 , ( Vector3.left + Vector3.back ).normalized * 6f , ( Vector3.left + Vector3.back ).normalized , null );
		MakeBranch( 2 , ( Vector3.right + Vector3.back ).normalized * 6f , ( Vector3.right + Vector3.back ).normalized , null );

        //outer routers
        MakeBranch( 3 , Vector3.left * 10f , Vector3.left , null );
        MakeBranch( 3 , Vector3.right * 10f , Vector3.right , null );

        //corners
        MakeBranch( 1 , ( Vector3.left * 9f ) + ( Vector3.forward * 9.75f ) , Vector3.left , null );
        MakeBranch( 1 , ( Vector3.right * 9f ) + ( Vector3.forward * 9.75f ) , Vector3.right , null );
        MakeBranch( 1 , ( Vector3.left * 9f ) + ( Vector3.back * 9.75f ) , Vector3.left , null );
        MakeBranch( 1 , ( Vector3.right * 9f ) + ( Vector3.back * 9.75f ) , Vector3.right , null );

        foreach ( Node node in nodes )
        {
            if ( node.nodeType != "target" && node.nodeType != "blackice" )
                node.AddRandomConnections();
        }
        
        foreach( Node node in nodes )
        {
			while ( node.nodeType != "target" && node.nodeType != "blackice" && node.connections.Count < 2 )
				node.AddRandomConnections();
        }

        Vector3 startPosition = Vector3.zero;
        int startCorner = Random.Range( 0 , 2 );

        switch ( startCorner )
        {
            case 0:

                startPosition = new Vector3( 15f , 0f , -10f );

                break;

            case 1:

                startPosition = new Vector3( -15f , 0f , -10f );

                break;

            case 2:

                startPosition = new Vector3( 15f , 0f , 10f );

                break;

            case 3:

                startPosition = new Vector3( -15f , 0f , 10f );

                break;
        }

        List< Node > connectionsToCull = new List<Node>();

        foreach( Node node in targetNodeScript.connections )
        {
            if ( node.nodeType != "blackice" )
                connectionsToCull.Add( node );
        }

        foreach( Node node in connectionsToCull )
        {
            node.connections.Remove( targetNodeScript );
            targetNodeScript.connections.Remove( node );
        }

        foreach ( Node node in nodes )
        {
            if ( node.nodeType != "target" )
                node.HideNode();
        }

        Collider[] homeNodeCandidates = Physics.OverlapSphere( startPosition , 5f );

        int homeNode = Random.Range( 0 , homeNodeCandidates.Length );

        Node homeNodeScript = homeNodeCandidates[ homeNode ].GetComponent<Node>();

        homeNodeScript.SetHome();
	}

    public bool targetBreached = false;

    public void EndGame ()
    {
        StartCoroutine( EndGame_Service() );
    }

    public GameObject uicam;

    private IEnumerator EndGame_Service ()
    {
        glitchIntensity = 0f;
        uicam.SetActive( true );

        GlitchEffect glitch = uicam.GetComponent< GlitchEffect >();
        glitch.enabled = true;

        Go.to( this , 5f , new GoTweenConfig().floatProp( "glitchIntensity" , 5f ).onComplete( c => Application.LoadLevel( 0 ) ) );

        while ( true )
        {
            glitch.intensity = glitchIntensity;

            yield return null;
        }

    }

    public float glitchIntensity { get; set; }

    #endregion

    #region Update

	void Update ()
    {
		if ( Input.GetKey( KeyCode.Escape ))
			EndGame();
    
        Vector3 mouseScreenPos = Input.mousePosition;
        mouseScreenPos.z = 40f;
        Vector3 mousePos = _mainCamera.ScreenToWorldPoint( mouseScreenPos );

        _mouseLight.transform.position = mousePos;

        if ( Input.GetMouseButtonDown( 0 ) )
        {
            RaycastHit hit;

            if ( Physics.Raycast( _mainCamera.ScreenPointToRay( Input.mousePosition ) , out hit ) )
            {
                if ( hit.transform.gameObject != _selectedNode && hit.transform.GetComponent< Node >().playerControlled )
                {
                    _selectedNode = hit.transform.gameObject;

                    _selectedNode.GetComponent<Node>().SelectNode();
                    _lineRenderer.enabled = true;
                    _lineRenderer.SetVertexCount( 2 );
                }
            }
        }

        if ( Input.GetMouseButton( 0 ) )
        {
            RaycastHit hit;

            if ( Physics.Raycast( _mainCamera.ScreenPointToRay( Input.mousePosition ) , out hit ) )
            {
                GameObject nodeToDestroy = hit.transform.gameObject;

                if ( !unlinkingNode && _selectedNode == nodeToDestroy.gameObject )
                {
                    Node toDestroy = nodeToDestroy.GetComponent< Node >();

                    if ( !toDestroy.home )
                    {
                        unlinkingNode = true;
                        float wait = 0f;

                        switch ( toDestroy.nodeType )
                        {
                            case "botnet":

                                _nodeDestroyTime = Time.time + 4f;
                                wait = 4f;

                                break;

                            case "router":

                                _nodeDestroyTime = Time.time + 4f;
                                wait = 4f;

                                break;

                            case "firewall":

                                _nodeDestroyTime = Time.time + 4f;
                                wait = 4f;

                                break;
                        }

                        toDestroy.UnlinkNode( wait );
                    }
                }
            }
            else
            {
                unlinkingNode = false;
                _nodeDestroyTime = 0f;
            }
        }

        if ( Input.GetMouseButtonUp( 0 ) && _selectedNode != null )
        {
            Node selectedNodeScript = _selectedNode.GetComponent<Node>();

            RaycastHit hit;

            if ( Physics.Raycast( _mainCamera.ScreenPointToRay( Input.mousePosition ) , out hit ) )
            {
                Node targetNodeScript = hit.transform.GetComponent< Node >();

                if ( targetNodeScript.connections.Contains( selectedNodeScript ) && !selectedNodeScript.links.Contains( targetNodeScript ) )
                {
                    selectedNodeScript.AddLink( targetNodeScript );
                }
            }

            selectedNodeScript.DeselectNode();
            _selectedNode = null;
            _lineRenderer.enabled = false;
            _lineRenderer.SetVertexCount( 0 );
            unlinkingNode = false;
            _nodeDestroyTime = 0f;
        }

        if ( _selectedNode != null )
        {
            _lineRenderer.SetPosition( 0 , _selectedNode.transform.position + ( Vector3.up * 0.5f ) );

            mouseScreenPos = Input.mousePosition;

            mouseScreenPos.z = 35f;

            mousePos = _mainCamera.ScreenToWorldPoint( mouseScreenPos );

            _lineRenderer.SetPosition( 1 , mousePos );
        }
	}

    #endregion

    #region Variables

	private AudioController c_audioController;
	private AudioController _audioController
	{
		get
		{
			if ( !c_audioController )
				c_audioController = GameObject.FindGameObjectWithTag( "AudioController" ).GetComponent< AudioController >();
			
			return c_audioController;
		}
	}

    [SerializeField] private GameObject nodePrefab;
    [ HideInInspector ] public bool unlinkingNode;
    private float _nodeDestroyTime = 0f;

    [HideInInspector] public List< Node > nodes = new List<Node>();
    [ HideInInspector ] public List< GameObject > nodeGO = new List<GameObject>();

    public float nodeOffset = 2;
    [SerializeField] private GameObject _mouseLight;
    private GameObject _selectedNode;

    private LineRenderer c_lineRenderer;
    private LineRenderer _lineRenderer
    {
        get
        {
            if ( !c_lineRenderer )
                c_lineRenderer = GetComponent<LineRenderer>();

            return c_lineRenderer;
        }
    }

    private Camera c_mainCamera;
    private Camera _mainCamera
    {
        get
        {
            if ( !c_mainCamera )
                c_mainCamera = GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Camera>();

            return c_mainCamera;
        }
    }

    #endregion
}
