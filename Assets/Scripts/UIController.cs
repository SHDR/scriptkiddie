﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

    public GameObject killer;
    public GameObject app;
    public GameObject score;
    public GameObject baseNode;
    public Camera UICamera;
    public Camera mainCamera;

	void Start ()
    {
        alpha = 1f;
	}

    public bool gameStarted = false;
    public float alpha { get; set; }

	void Update ()
    {
	    if ( !gameStarted && Input.GetMouseButtonDown(  0 ) )
        {
            gameStarted = true;

            Go.to( this , 2f , new GoTweenConfig().floatProp( "alpha" , 0f ).setEaseType( GoEaseType.BounceOut ).onComplete( c => StartCoroutine( StartGame() ) ) );
        }
        else if ( gameStarted && alpha > 0f )
        {
            _killerText.color = new Color( _killerText.color.r , _killerText.color.g , _killerText.color.b , alpha );
            _appText.color = new Color( _killerText.color.r , _killerText.color.g , _killerText.color.b , alpha );
        }
        else
        {
            _glitchEffect.intensity = Mathf.PingPong( Time.time * 0.25f , 1.5f );
        }
	}

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds( 0.75f );

        baseNode.SetActive( true );
        UICamera.gameObject.SetActive( false );
        _glitchEffect.enabled = false;
        score.SetActive( true );
    }

    private GlitchEffect c_glitchEffect;
    private GlitchEffect _glitchEffect
    {
        get
        {
            if ( !c_glitchEffect )
                c_glitchEffect = UICamera.GetComponent<GlitchEffect>();

            return c_glitchEffect;
        }
    }

    private Text c_killerText;
    private Text _killerText
    {
        get
        {
            if ( !c_killerText )
                c_killerText = killer.GetComponent<Text>();

            return c_killerText;
        }
    }

    private Text c_appText;
    private Text _appText
    {
        get
        {
            if ( !c_appText )
                c_appText = app.GetComponent<Text>();

            return c_appText;
        }
    }
}
