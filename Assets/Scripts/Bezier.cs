﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

static class Bezier
{
	public static Vector3 CubicBezierSolver(Vector3[] points, float t, int offset = 0)
	{
		float d = 1f - t;
		float dd = d * d;
		float tt = t * t;
		return dd * d * points[offset + 0] + 3f * dd * t * points[offset + 2] + 3f * d * tt * points[offset + 3] + tt * t * points[offset + 1];
	}

	public static void DrawCircle_LineRenderer ( LineRenderer lineRenderer , float resolution , Vector3[] beziers , float fillAmount )
	{
		int vertCount = ( int ) ( ( ( 4 * resolution ) + 1 ) * fillAmount );
		lineRenderer.SetVertexCount( vertCount );

		int drawCount = 0;
		float drawTime = 0f;
		Vector3 startPos = Vector3.zero;
		
		if ( vertCount > 1 )
		{
			Vector3 position = Bezier.CubicBezierSolver( beziers , drawTime , 0 );
			
			for ( int i = 0 ; i < 4 ; i++ )
			{
				for ( int j = 0 ; j < resolution ; j++ )
				{
					if ( drawCount < vertCount )
					{
						position = lineRenderer.transform.position + Bezier.CubicBezierSolver( beziers , drawTime , i * 4 );
						
						if ( drawCount == 0 )
							startPos = position;
						
						lineRenderer.SetPosition( drawCount , position );
						
						drawTime += 1f / resolution;
						drawCount++;
					}
				}
				
				drawTime = 0f;
			}
			
			if ( fillAmount == 1f )
				lineRenderer.SetPosition( vertCount - 1, startPos );
		}
	}

	public static void InstanceObjects_Circle ( string objectName , float radius , float instanceDensityDivider , List< Transform > instancedObjects , Vector3[] circle , Transform parent , bool fillFromBottom )
	{
		float circumference = 2 * ( Mathf.PI * Mathf.Abs( radius ) );

		if ( circumference < 0 )
			Debug.Log( "Circumference is fucked" );

		int instanceCount = Mathf.CeilToInt( circumference );

		if ( instanceCount < 0 )
			Debug.Log( "Circumference: " + circumference + " instanceCount: " + instanceCount );

		if ( instanceDensityDivider < 1f )
			instanceDensityDivider = 1f;

		instanceCount = ( int ) ( instanceCount / instanceDensityDivider );

		if ( fillFromBottom && radius > 0 )
			radius = -radius;

		if ( instanceCount > instancedObjects.Count  )
		{
			int addCount = instancedObjects.Count;
			while ( addCount < instanceCount )
			{
				if ( instancedObjects.Count < 1 )
				{
					//instancedObjects.Add( Metronome.Instance.Spawn( objectName , CubicBezierSolver( circle , 0f ) , Quaternion.identity ).transform ); -- INSTANTIATE
				}
				//else
					//instancedObjects.Add( Metronome.Instance.Spawn( objectName , instancedObjects[ 0 ].position , Quaternion.identity ).transform ); -- INSTANTIATE
				
				instancedObjects[ addCount ].parent = parent;
				addCount++;
			}
		}
		else if ( instanceCount < instancedObjects.Count )
		{
			for ( int i = instanceCount ; i < instancedObjects.Count ; i++ )
			{
				//if ( i < instancedObjects.Count && instancedObjects[ i ] != null )
					//Metronome.Instance.Despawn( instancedObjects[ i ].gameObject ); -- DESTROY
			}

			instancedObjects.RemoveRange( instanceCount , instancedObjects.Count - instanceCount );
		}
	}

	public static void PositionObjects_Circle ( Vector3 center , float radius , List< Transform > objectsToPosition , Vector3[] circle , float instancePosSpeed , float instanceDensityDivider )
	{
		int posCount = 0;
		int bezCount = 0;
		float circumference = 2 * ( Mathf.PI * Mathf.Abs( radius ) );
		int instancesPerBezier = ( int ) ( circumference / 4f );
		float remainder = circumference % 4;
		float positionTime = 0;

		if ( instanceDensityDivider < 1f )
			instanceDensityDivider = 1f;

		while ( posCount < objectsToPosition.Count )
		{
			if ( positionTime > 1f )
			{
				bezCount++;
				if ( bezCount > 3 )
				{
					bezCount = 3;
					positionTime = 1f;
				}
				else
					positionTime -= 1f;
			}
			
			Vector3 position = Bezier.CubicBezierSolver( circle , positionTime , bezCount * 4 );
			//Debug.Log( "Center: " + center + " Position: " + position );
			position += center;
			Vector3 newPos = Vector3.Lerp( objectsToPosition[ posCount ].position , position , ( instancePosSpeed + Vector3.Distance( objectsToPosition[ posCount ].position , position ) ) * Time.deltaTime );
			
			//if ( position.x == newPos.x )
			//	Debug.Log( "Center: " + center + " Radius: " + radius + " Position: " + position + " NewPos: " + newPos );

		 	if ( newPos == newPos ) // null check
		 		objectsToPosition[ posCount ].position = newPos;
			
			
			objectsToPosition[ posCount ].rotation = Quaternion.LookRotation( center - newPos , Vector3.right );
			
			posCount++;
			positionTime += 1f / ( ( instancesPerBezier + ( remainder / 4f ) ) / instanceDensityDivider );
		}
	}

	public static Vector3[] UpdateCubicBezierCircle(Vector3[] bezier, float radius, float yPosition )
	{
		float innerRadius = 0.551915024494f * radius;
		
		//clockwise
		//first
        bezier[ 0 ] = new Vector3( 0 , yPosition , radius );
        bezier[ 1 ] = new Vector3( radius , yPosition , 0 );
        bezier[ 2 ] = new Vector3( innerRadius , yPosition , radius );
        bezier[ 3 ] = new Vector3( radius , yPosition , innerRadius );

		//second
        bezier[ 4 ] = new Vector3( radius , yPosition , 0 );
        bezier[ 5 ] = new Vector3( 0 , yPosition , -radius );
        bezier[ 6 ] = new Vector3( radius , yPosition , -innerRadius );
        bezier[ 7 ] = new Vector3( innerRadius , yPosition , -radius );

		//third
        bezier[ 8 ] = new Vector3( 0 , yPosition , -radius );
        bezier[ 9 ] = new Vector3( -radius , yPosition , 0 );
        bezier[ 10 ] = new Vector3( -innerRadius , yPosition , -radius );
        bezier[ 11 ] = new Vector3( -radius , yPosition , -innerRadius );

		//fourth
        bezier[ 12 ] = new Vector3( -radius , yPosition , 0 );
        bezier[ 13 ] = new Vector3( 0 , yPosition , radius );
        bezier[ 14 ] = new Vector3( -radius , yPosition , innerRadius );
        bezier[ 15 ] = new Vector3( -innerRadius , yPosition , radius );

		return bezier;
	}
}
