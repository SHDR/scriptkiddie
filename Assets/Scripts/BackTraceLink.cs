﻿using UnityEngine;
using System.Collections;

public class BackTraceLink : MonoBehaviour 
{
    public int indexA;
    public int indexB;
    
    [ HideInInspector ] public Vector3 posA;
    [ HideInInspector ] public Vector3 posB;
    
    public void DrawConnection ( Vector3 posA , Vector3 posB )
    {
        this.posA = posA;
        this.posB = posB;

        _lineRenderer.SetVertexCount( 2 );
        _lineRenderer.SetPosition( 0 , posA );
        _lineRenderer.SetPosition( 1 , posB );
    }

    private LineRenderer c_lineRenderer;
    private LineRenderer _lineRenderer
    {
        get
        {
            if ( !c_lineRenderer )
                c_lineRenderer = GetComponent< LineRenderer >();

            return c_lineRenderer;
        }
    }
}
