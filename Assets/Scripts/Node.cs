﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Node : MonoBehaviour
{
	#region Audio

	private AudioController c_audioController;
	private AudioController _audioController
	{
		get
		{
			if ( !c_audioController )
				c_audioController = GameObject.FindGameObjectWithTag( "AudioController" ).GetComponent< AudioController >();
				
			return c_audioController;
		}
	}

	public AudioClip nodeConnectStart;
	public AudioClip nodeConnectLoop;
	public AudioClip nodeConnectComplete;
	public AudioClip nodeDisconnect;
	public AudioClip traceStart;
	public AudioClip traceLoop;
	public AudioClip traceEnd;
	public AudioClip traceTakeOver;
	public AudioClip traceRelease;
	public AudioClip targetAssaultLoop;
	
	#endregion

    #region Visibility

    public void ShowNode()
    {
        if ( !playerControlled && nodeType != "target" )
        {
            if ( nodeType == "blackice" )
            {
                Go.to( transform , 0.4f , new GoTweenConfig().scale( Vector3.one * 1.5f , false ).setEaseType( GoEaseType.BackOut ) );
            }
            else
            {
                Go.to( transform , 0.4f , new GoTweenConfig().scale( Vector3.one , false ).setEaseType( GoEaseType.BackOut ) );
            }
        }
    }

    public void HideNode ()
    {
        transform.localScale = Vector3.zero;
    }

    #endregion

    #region Connections

    public void AddConnection ( Node newConnection )
    {
        connections.Add( newConnection );
    }

    public void AddRandomConnections ()
    {        
        while ( connections.Count < 2 )
        {
            Collider[] colliders = Physics.OverlapSphere( transform.position , 4.5f );

            int randomConnection = Random.Range( 0 , colliders.Length - 1 );

            Node randomNode = colliders[ randomConnection ].GetComponent<Node>();

            if ( randomNode != this && !connections.Contains( randomNode ) && randomNode.nodeType != "target" )
            {
                randomNode.AddConnection( this );

                AddConnection( randomNode );
            }
        }

        while ( Random.Range( 0f , 1f ) > 0.2f )
        {
            Collider[] colliders = Physics.OverlapSphere( transform.position , 4.5f );

            int randomConnection = Random.Range( 0 , colliders.Length - 1 );

            Node randomNode = colliders[ randomConnection ].GetComponent<Node>();

            if ( randomNode != this && !connections.Contains( randomNode ) )
            {
                randomNode.AddConnection( this );

                AddConnection( randomNode );
            }
        }
    }

    public void MakeConnections ()
    {
        foreach( Node connection in connections )
        {
            StartCoroutine( MakeConnection_Service( connection ) );
        }
    }

    private IEnumerator MakeConnection_Service( Node connection )
    {
        float wait = 0.5f;
        float progress = 0f;

        GameObject newConnection = Instantiate( nodeConnectionPrefab ) as GameObject;
        newConnection.transform.parent = transform;
        newConnection.transform.position = transform.position;

        NodeConnection newConnectionScript = newConnection.GetComponent<NodeConnection>();
        newConnectionScript.DrawConnection( transform.position , connection.transform.position );

        while ( progress < wait )
        {
            progress += Time.deltaTime;
            float time = ( progress - 0f ) / ( wait - 0f );

            newConnectionScript.DrawConnection( transform.position , Vector3.Lerp( transform.position , connection.transform.position , time ) );

            yield return null;
        }

        connection.ShowNode();
    }

    public void ShowConnectionHighlight()
    {
        StartCoroutine( "ShowConnection_Effect" );
    }

    private IEnumerator ShowConnection_Effect()
    {
        Material material = transform.GetChild( 0 ).GetComponent<Renderer>().material;

        while ( true )
        {
            material.SetColor( "_Color" , Color.Lerp( Color.red , Color.black , Mathf.PingPong( Time.time , 1f ) ) );
            material.SetColor( "_EmissionColor" , Color.Lerp( Color.red , Color.black , Mathf.PingPong( Time.time , 1f ) ) );
            material.SetColor( "_SpecularColor" , Color.Lerp( Color.red , Color.black , Mathf.PingPong( Time.time , 1f ) ) );
            yield return null;
        }
    }

    public void HideConnectionHighlight()
    {
        StopCoroutine( "ShowConnection_Effect" );

        if ( !playerControlled )
            transform.GetChild( 0 ).GetComponent<Renderer>().material.color = Color.black;
    }

    #endregion

    #region Links

    [HideInInspector] public bool linking = false;

    public void AddLink( Node toLink )
    {
        if ( !toLink.links.Contains( this ) )
        {
			_audioController.Play( nodeConnectStart , transform );
            linksInProgress++;
            float wait = 0f;

            switch ( toLink.nodeType )
            {
                case "botnet" :
                    wait = Random.Range( 2f , 2f );

                    break;

                case "router":
                    wait = Random.Range( 2f , 2f );

                    break;

                case "firewall":
                    wait = Random.Range( 2f , 2f );

                    break;

                case "blackice":
                    wait = Random.Range( 2f , 2f );

                    break;

                case "target":
                    wait = Random.Range( 2f , 2f );

                    break;
            }

            StartCoroutine( WaitForLink_Service( toLink , wait ) );

            if ( toLink.nodeType != "blackice" && toLink.nodeType != "target" )
                toLink.ShowLinkProgress( this , wait );
        }
    }

    public void UnlinkNode ( float wait )
    {
        StartCoroutine( UnlinkNode_Service( wait ) );
    }

    private IEnumerator UnlinkNode_Service( float wait )
    {
        float progress = wait;

        Go.killAllTweensWithTarget( transform.GetChild( 2 ) );
        Go.to( transform.GetChild( 2 ) , 0.2f , new GoTweenConfig().scale( new Vector3( 3f , 0.5f , 3f ) ).setEaseType( GoEaseType.BackOut ).onComplete( c =>
            Go.to( transform.GetChild( 2 ) , 0.8f , new GoTweenConfig().scale( Vector3.zero ).setEaseType( GoEaseType.BounceOut ) )
            ) );

        _lineRenderer.enabled = true;
        Bezier.UpdateCubicBezierCircle( bezierCircle , 1f , -2f );
        
        _audioController.Play( nodeConnectStart , transform.position );
        AudioSource loop = _audioController.PlayNoDestroy( nodeConnectLoop , transform.position , 1f , 1f );
		loop.loop = true;
		
        while (  progress > 0f && _nodeBase.unlinkingNode )
        {
            progress -= Time.deltaTime;
            float time = ( progress - 0f ) / ( wait - 0f );
            Bezier.DrawCircle_LineRenderer( _lineRenderer , 40f , bezierCircle , time );

            if ( progress <= 0f )
            {
                _lineRenderer.enabled = false;
                _lineRenderer.SetVertexCount( 0 );
            }

            yield return null;
        }
		
		Destroy( loop.gameObject );
		_audioController.Play( nodeConnectComplete , transform.position );
		
        yield return null;

        if ( !_nodeBase.unlinkingNode )
        {
            while ( progress < wait )
            {
                progress += Time.deltaTime;
                float time = ( progress - 0f ) / ( wait - 0f );
                Bezier.DrawCircle_LineRenderer( _lineRenderer , 40f , bezierCircle , time );

                if ( progress >= wait )
                {
                    _lineRenderer.enabled = false;
                    _lineRenderer.SetVertexCount( 0 );
                }

                yield return null;
            }

            Go.killAllTweensWithTarget( transform.GetChild( 2 ) );
            transform.GetChild( 2 ).localScale = new Vector3( 3f , 0.5f , 3f );
            Go.to( transform.GetChild( 2 ) , 0.4f , new GoTweenConfig().scale( new Vector3( 2.25f , 0.5f , 2.25f ) , false ).setEaseType( GoEaseType.BounceOut ) );

        }
        else
        {
            foreach ( Node linkedNode in links )
            {
                if ( linkedNode.links.Count < 1 )
                    linkedNode.RemovePlayerControl();
            }

            foreach( NodeLink link in GetComponentsInChildren< NodeLink >() )
            {
                StartCoroutine( RemoveLink_Service( link ) );                
            }

            foreach( Node connection in connections )
            {
                if ( connection.links.Contains( this ) )
                {
                    foreach ( NodeLink link in connection.GetComponentsInChildren< NodeLink >() )
                    {
                        if ( link.posB + ( Vector3.up * 0.25f ) == transform.position )
                        {
                            StartCoroutine( RemoveLink_Service( link ) );
                        }
                    }

                    connection.links.Remove( this );
                }
            }

            foreach( Node node in _nodeBase.nodes )
                RemovePlayerControl();
        }
    }

    private IEnumerator RemoveLink_Service ( NodeLink link )
    {
        float progress = 1f;

        _lineRenderer.enabled = true;
        Bezier.UpdateCubicBezierCircle( bezierCircle , 1f , -2f );

        while ( progress > 0f )
        {
            progress -= Time.deltaTime;
            link.DrawConnection( link.posA , Vector3.Lerp( link.posA , link.posB , progress) );

            if ( progress <= 0f )
            {
                _lineRenderer.enabled = false;
                _lineRenderer.SetVertexCount( 0 );
            }

            yield return null;
        }

        Destroy( link.gameObject );

        linking = false;
    }

    public void ShowLinkProgress( Node linkingNode , float wait )
    {
        if ( !linking )
        {
            linking = true;
            StartCoroutine( LinkProgress_Service( linkingNode , wait ) );
        }
    }

    private IEnumerator LinkProgress_Service( Node linkingNode , float wait )
    {
        float progress = 0f;

        _lineRenderer.enabled = true;
        Bezier.UpdateCubicBezierCircle( bezierCircle , 1.15f , -1f );

		AudioSource loop = _audioController.PlayNoDestroy( nodeConnectLoop , transform.position , 1f , 1f );
		loop.loop = true;
		
        while ( progress < wait )
        {
            progress += Time.deltaTime / linkingNode.linksInProgress;
            float time = ( progress - 0f ) / ( wait - 0f );
            Bezier.DrawCircle_LineRenderer( _lineRenderer , 40f , bezierCircle , time );

            if ( progress >= wait )
            {
                _lineRenderer.enabled = false;
                _lineRenderer.SetVertexCount( 0 );
            }

            yield return null;
        }
        
        Destroy( loop.gameObject );
        
        _audioController.Play( nodeConnectComplete , transform );

        linking = false;
    }

    private IEnumerator WaitForLink_Service( Node toLink , float wait )
    {
        float progress = 0f;

        GameObject newLink = Instantiate( nodeLinkPrefab ) as GameObject;
        newLink.transform.parent = transform;
        newLink.transform.position = transform.position;

        NodeLink linkScript = newLink.GetComponent<NodeLink>();

        while ( progress < wait )
        {
            progress += Time.deltaTime / linksInProgress;
            float time = ( progress - 0f ) / ( wait - 0f );

            linkScript.DrawConnection( transform.position + ( Vector3.down * 0.25f ) , Vector3.Lerp( transform.position + ( Vector3.down * 0.25f ) , toLink.transform.position + ( Vector3.down * 0.25f ) , time ) );

            yield return null;
        }

        linksInProgress--;

        linking = false;
        links.Add( toLink );

        if ( toLink.nodeType != "target" && toLink.nodeType != "blackice" )
            toLink.SetPlayerControl();
    }

    #endregion

    #region Packets

    private IEnumerator SendPacket_Service ()
    {
        while ( true )
        {
            if ( playerControlled && links.Count > 0 )
            {
                Node targetNode = links[ packetTarget ];
                GameObject packet = Instantiate( packetPrefab , transform.position + ( Vector3.up * 0.5f ) , Quaternion.identity ) as GameObject;
                Packet packetScript = packet.GetComponent<Packet>();
                packetScript.transform.parent = links[ packetTarget ].transform;
                packetScript.SetTargetNode( targetNode );

                yield return new WaitForSeconds( sendPacketInterval );

                packetTarget++;

                if ( packetTarget >= links.Count )
                    packetTarget = 0;
            }
            else
            {
                yield return null;
            }
        }
    }

    public void RoutePacket()
    {
        StartCoroutine( RoutePacket_Service() );
    }

    private IEnumerator RoutePacket_Service ()
    {
        packets++;
        bool skip = false;
        int iterations = 1;

        switch ( nodeType )
        {
            case "firewall" :

                if ( load > capacity )
                {
                    skip = true;

                    if ( !backTracing )
                    {
                        foreach ( Node node in connections )
                        {
                            if ( node.playerControlled )
                            {
                                BackTrace( node );
                            }
                        }

                        Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_Color" ).setEaseType( GoEaseType.ExpoOut ) );
                        Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_SpecularColor" ).setEaseType( GoEaseType.ExpoOut ) );
                        Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_EmissionColor" ).setEaseType( GoEaseType.ExpoOut ) );

                        backTracing = true;
                        _audioController.Play( traceStart , transform.position );
                    }
                }

                break;

            case "blackice":
                
                if ( capacity > load && !playerControlled )
                {
                    if ( !_particles )
                    {
                        _particles = Instantiate( blackiceAssaultParticles , transform.position , Quaternion.LookRotation( Vector3.up ) ) as GameObject;
                        _particleChild1 = _particles.transform.GetChild( 0 ).GetComponent<ParticleSystem>();
                        _particleChild2 = _particles.transform.GetChild( 1 ).GetComponent<ParticleSystem>();
                        _particleChild3 = _particles.transform.GetChild( 2 ).GetComponent<ParticleSystem>();
                    }

                    _particleChild1.Emit( 4 );
                    _particleChild2.Emit( 4 );
                    _particleChild3.Emit( 4 );

                    skip = true;
                }
                else if ( !playerControlled )
                {
                    Destroy( _particles );
                    _particles = null;
                    SetPlayerControl();

                    foreach( Node node in connections )
                    {
                        if ( node.playerControlled )
                        {
                            BackTrace( node );
                        }
                    }

                    foreach ( Node connection in connections )
                    {
                        if ( connection.nodeType == "target" )
                            AddLink( connection );
                    }

                    Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_Color" ).setEaseType( GoEaseType.ExpoOut ) );
                    Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_SpecularColor" ).setEaseType( GoEaseType.ExpoOut ) );
                    Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_EmissionColor" ).setEaseType( GoEaseType.ExpoOut ) );

                    _audioController.Play( traceStart , transform.position );
                    backTracing = true;
                }
                
                break;

            case "botnet":

                iterations = 2;

                break;

            case "router" :

                if ( load > capacity )
                {
                    skip = true;
                }
                else
                    iterations = links.Count;

                break;

            case "target" :

                if ( !_nodeBase.targetBreached )
                {
				    _audioController.Play( targetAssaultLoop , transform.position );
                    _nodeBase.targetBreached = true;
                    _particles = Instantiate( targetAssaultParticles );
                    _particleChild1 = _particles.transform.GetChild( 1 ).GetComponent< ParticleSystem >();
                    _particleChild2 = _particles.transform.GetChild( 2 ).GetComponent< ParticleSystem >();
                    _particleChild3 = _particles.transform.GetChild( 3 ).GetComponent< ParticleSystem >();
                }

                _particleChild1.Emit( 4 );
                _particleChild2.Emit( 4 );
                _particleChild3.Emit( 4 );

                score++;

                char[] currentScore = _scoreText.text.ToCharArray();

                char[] newScore = score.ToString().ToCharArray();

                for ( int i = currentScore.Length - 1 ; i >= 0 ; i-- )
                {
                    if ( i - ( currentScore.Length - newScore.Length ) >= 0 )
                    currentScore[ i ] = newScore[ i - ( currentScore.Length - newScore.Length ) ];
                }

                string finalScore = new string( currentScore );

                _scoreText.text = finalScore;

                Material baseMat = transform.GetChild( 1 ).GetComponent<Renderer>().material;
                Material indicatorMat = transform.GetChild( 2 ).GetComponent<Renderer>().material;

                if ( baseMat.color == Color.black )
                {
                    baseMat.SetColor( "_Color" , Color.red );
                    baseMat.SetColor( "_SpecularColor" , Color.red );
                    baseMat.SetColor( "_EmissionColor" , Color.red );

                    indicatorMat.SetColor( "_Color" , Color.black );
                    indicatorMat.SetColor( "_SpecularColor" , Color.black );
                    indicatorMat.SetColor( "_EmissionColor" , Color.black );

                    if ( transform.GetChild( 2 ).localScale.magnitude > new Vector3( 3.5f , 0.5f , 3.5f ).magnitude )
                        transform.GetChild( 2 ).localScale = new Vector3( 2f , 0.5f , 2f );

                    Go.killAllTweensWithTarget( transform.GetChild( 2 ) );
                    Go.to( transform.GetChild( 2 ) , 0.2f , new GoTweenConfig().scale( new Vector3( 3f , 0.5f , 3f ) ).setEaseType( GoEaseType.Punch ) );
                }
                else
                {
                    baseMat.SetColor( "_Color" , Color.black );
                    baseMat.SetColor( "_SpecularColor" , Color.black );
                    baseMat.SetColor( "_EmissionColor" , Color.black );

                    indicatorMat.SetColor( "_Color" , Color.red );
                    indicatorMat.SetColor( "_SpecularColor" , Color.red );
                    indicatorMat.SetColor( "_EmissionColor" , Color.red );

                    if ( transform.GetChild( 1 ).localScale.magnitude > new Vector3( 2f , 0.5f , 2f ).magnitude )
                        transform.GetChild( 1 ).localScale = new Vector3( 1f , 0.5f , 1f );

                    Go.killAllTweensWithTarget( transform.GetChild( 1 ) );
                    Go.to( transform.GetChild( 1 ) , 0.2f , new GoTweenConfig().scale( new Vector3( 2f , 0.5f , 2f ) ).setEaseType( GoEaseType.Punch ) );

                }

                break;
        }

        if ( !skip && playerControlled && links.Count > 0 )
        {
            for ( int i = 0 ; i < iterations ; i++ )
            {
                if ( packetTarget < links.Count )
                {
                    Node targetNode = links[ packetTarget ];
                    GameObject packet = Instantiate( packetPrefab , transform.position + ( Vector3.up * 0.5f ) , Quaternion.identity ) as GameObject;
                    Packet packetScript = packet.GetComponent<Packet>();
                    packetScript.transform.parent = links[ packetTarget ].transform;
                    packetScript.SetTargetNode( targetNode );
                }
                
                packetTarget++;

                if ( packetTarget >= links.Count )
                    packetTarget = 0;

                if ( nodeType != "router" )
                    yield return new WaitForSeconds( sendPacketInterval / iterations );
            }
        }
    }

    static int score = 0;
    public GameObject targetAssaultParticles;
    public GameObject blackiceAssaultParticles;

    private GameObject _particles = null;
    private ParticleSystem _particleChild1;
    private ParticleSystem _particleChild2;
    private ParticleSystem _particleChild3;

    private Text c_scoreText;
    private Text _scoreText
    {
        get
        {
            if ( !c_scoreText )
                c_scoreText = GameObject.FindGameObjectWithTag( "Score" ).GetComponent<Text>();

            return c_scoreText;
        }
    }

    public void WatchPackets()
    {
        StartCoroutine( WatchPackets_Service() );
    }

    private IEnumerator WatchPackets_Service()
    {
        _lineRenderer.SetVertexCount( 0 );
        _lineRenderer.enabled = true;

        while ( playerControlled || ( nodeType == "blackice" || nodeType == "target" ) )
        {
            if ( transform.GetChild( 2 ).localScale != Vector3.zero )
            {
                if ( !_lineRenderer.enabled )
                {
                    _lineRenderer.enabled = true;
                }

                if ( nodeType == "blackice" || nodeType == "target" )
                    Bezier.UpdateCubicBezierCircle( bezierCircle , 1.5f , -1f );
                else if ( !linking )
                    Bezier.UpdateCubicBezierCircle( bezierCircle , 0.6f , 0.5f );

                yield return new WaitForSeconds( 1f );

                if ( packets > load )
                {
                    load = packets;
                    updateLoad = true;
                }
                else if ( updateLoad && load > packets )
                {
                    load = packets;
                }

                packets = 0;

                float time = load / capacity;

                if ( time > 1f )
                    time = 1f;

                Bezier.DrawCircle_LineRenderer( _lineRenderer , 40f , bezierCircle , time );
            }
            else
            {
                yield return new WaitForSeconds( 1f );

                if ( packets > load )
                {
                    load = packets;
                    updateLoad = true;
                }
                else if ( updateLoad && load > packets )
                {
                    load = packets;
                }

                packets = 0;

            }
                yield return null;
        }

        _lineRenderer.enabled = false;
    }

    #endregion

    #region Backtrace

    public bool backTracing = false;
    public int backTracesInProgress = 0;


    public void BackTrace ( Node toTrace )
    {
        if ( !toTrace.backTracing )
        {
            backTracing = true;
            backTracesInProgress++;
            float wait = 0f;

            switch ( toTrace.nodeType )
            {
                case "botnet":
                    wait = 2f;

                    break;

                case "router":
                    wait = 2f;

                    break;

                case "firewall":
                    wait = 2f;

                    break;
            }

            StartCoroutine( BackTrace_Service( toTrace , wait ) );
        }
    }

    private IEnumerator BackTrace_Service ( Node toTrace , float wait )
    {
        float progress = 0f;

        GameObject newBacktrace = Instantiate( backTraceLinkPrefab ) as GameObject;
        newBacktrace.transform.parent = transform;
        newBacktrace.transform.position = transform.position;

        BackTraceLink traceScript = newBacktrace.GetComponent<BackTraceLink>();

        while ( progress < wait )
        {
            progress += Time.deltaTime / backTracesInProgress;
            float time = ( progress - 0f ) / ( wait - 0f );

            traceScript.DrawConnection( transform.position + ( Vector3.up * 0.1f ) , Vector3.Lerp( transform.position + ( Vector3.up * 0.1f ) , toTrace.transform.position + ( Vector3.up * 0.1f ) , time ) );

            yield return null;
        }

        if ( toTrace.home )
            _nodeBase.EndGame();

		_audioController.Play( traceTakeOver , transform.position );
        int linksToTrace = 0;

        foreach ( Node nextNode in toTrace.connections )
        {
            if ( nextNode.playerControlled && !nextNode.backTracing && nextNode.links.Contains( toTrace ) )
            {
                toTrace.BackTrace( nextNode );
                linksToTrace++;
            }
        }

        if ( linksToTrace == 0 && !_nodeBase.targetBreached )
        {
            Go.to( toTrace.transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_Color" ).setEaseType( GoEaseType.ExpoOut ) );
            Go.to( toTrace.transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_SpecularColor" ).setEaseType( GoEaseType.ExpoOut ) );
            Go.to( toTrace.transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_EmissionColor" ).setEaseType( GoEaseType.ExpoOut ) );

            yield return new WaitForSeconds( 3f );
            RemoveBackTrace();
        }
        else
        {
            Go.to( toTrace.transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_Color" ).setEaseType( GoEaseType.ExpoOut ) );
            Go.to( toTrace.transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_SpecularColor" ).setEaseType( GoEaseType.ExpoOut ) );
            Go.to( toTrace.transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_EmissionColor" ).setEaseType( GoEaseType.ExpoOut ) );
        }

        backTracesInProgress--;
    }

    public void RemoveBackTrace ()
    {
        backTracing = false;

        foreach ( BackTraceLink trace in transform.GetComponentsInChildren<BackTraceLink>() )
        {
            Node traceTarget = null;

            foreach ( Node node in connections )
            {
                if ( node.transform.position + ( Vector3.up * 0.1f ) == trace.posB )
                    traceTarget = node;
            }

            if ( traceTarget != null )
            {
                StartCoroutine( RemoveConnectedBackTraces() );
                StartCoroutine( RemoveBackTrace_Service( trace , traceTarget ) );
            }
        }
    }

    private IEnumerator RemoveConnectedBackTraces ()
    {
        yield return new WaitForSeconds( 1f );
		
		_audioController.Play( traceRelease , transform.position );

        CleanUpLinks();
        RemovePlayerControl();

        foreach ( Node node in connections )
        {
            if ( node.GetComponentsInChildren< BackTraceLink >().Length > 0 )
            {
                node.RemoveBackTrace();
            }
        }
    }

    private void CleanUpLinks ()
    {
        foreach ( NodeLink link in GetComponentsInChildren< NodeLink >() )
        {
            StartCoroutine( CleanUpLinks_Service( link ) );
        }
    }

    private IEnumerator CleanUpLinks_Service ( NodeLink link )
    {
        float progress = 1f;

        while ( progress > 0f )
        {
            progress -= Time.deltaTime;

            if ( link != null )
                link.DrawConnection( link.posA , Vector3.Lerp( link.posA , link.posB , progress ) );

            yield return null;
        }

        if ( link != null )
            Destroy( link.gameObject );
    }

    private IEnumerator RemoveBackTrace_Service( BackTraceLink trace , Node traceTarget )
    {
        float progress = 1f;

        traceTarget.CleanUpLinks();
        traceTarget.RemovePlayerControl();

        while ( progress > 0f )
        {
            progress -= Time.deltaTime;

            trace.DrawConnection( transform.position + ( Vector3.up * 0.1f ) , Vector3.Lerp( transform.position + ( Vector3.up * 0.1f ) , traceTarget.transform.position + ( Vector3.up * 0.1f ) , progress ) );

            yield return null;
        }
    }

    #endregion

    #region Player control

    [ HideInInspector ] public bool home = false;

    public void SetHome ()
    {
        SetNodeType( "home" );
        home = true;
        SetPlayerControl();

        transform.localScale = Vector3.one * 1.5f;
        StartCoroutine( "SendPacket_Service" );
    }

    public void SetPlayerControl ()
    {
        if ( !playerControlled && nodeType != "target" )
        {
            if ( !home )
                ShowNode();
            
            if ( nodeType != "blackice" || nodeType != "target" )
                MakeConnections();

            playerControlled = true;

            if ( nodeType != "target" )
            {
                Go.killAllTweensWithTarget( transform.GetChild( 0 ) );
                Go.to( transform.GetChild( 0 ) , 0.2f , new GoTweenConfig().scale( new Vector3( 1f , 1f , 1f ) ).setEaseType( GoEaseType.BackOut ).onComplete( c =>
                    Go.to( transform.GetChild( 0 ) , 0.8f , new GoTweenConfig().scale( new Vector3( 0.5f , 0.5f , 0.5f ) ).setEaseType( GoEaseType.BounceOut ) )
                    ) );

                transform.GetChild( 0 ).GetComponent<Renderer>().material.SetColor( "_Color" , Color.red );
                transform.GetChild( 0 ).GetComponent<Renderer>().material.SetColor( "_EmissionColor" , Color.red );
                transform.GetChild( 2 ).GetComponent<Renderer>().material.SetColor( "_Color" , Color.red );
                transform.GetChild( 2 ).GetComponent<Renderer>().material.SetColor( "_EmissionColor" , Color.red );
                transform.GetChild( 2 ).GetComponent<Renderer>().material.SetColor( "_SpecularColor" , Color.red );

                Go.killAllTweensWithTarget( transform.GetChild( 2 ) );
                Go.to( transform.GetChild( 2 ) , 0.2f , new GoTweenConfig().scale( new Vector3( 3f , 0.5f , 3f ) ).setEaseType( GoEaseType.BackOut ).onComplete( c =>
                    Go.to( transform.GetChild( 2 ) , 0.8f , new GoTweenConfig().scale( new Vector3( 2.25f , 0.5f , 2.25f ) ).setEaseType( GoEaseType.BounceOut ) )
                    ) );

                if ( nodeType == "router" || nodeType == "firewall" )
                    WatchPackets();
            }
        }
    }

    public void RemovePlayerControl ()
    {
        links.Clear();
        transform.GetChild( 0 ).localScale = Vector3.one * 0.5f;

        Go.killAllTweensWithTarget( transform.GetChild( 2 ) );
        Go.to( transform.GetChild( 2 ) , 0.2f , new GoTweenConfig().scale( new Vector3( 3f , 0.5f , 3f ) ).setEaseType( GoEaseType.BackOut ).onComplete( c =>
                        Go.to( transform.GetChild( 2 ) , 0.8f , new GoTweenConfig().scale( Vector3.zero ).setEaseType( GoEaseType.BounceOut ) )
                        ) );

        Go.to( transform.GetChild( 0 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_Color" ).setEaseType( GoEaseType.ExpoOut ) );
        Go.to( transform.GetChild( 0 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_SpecularColor" ).setEaseType( GoEaseType.ExpoIn ) );
        Go.to( transform.GetChild( 0 ) , 0.4f , new GoTweenConfig().materialColor( Color.black , "_EmissionColor" ).setEaseType( GoEaseType.ExpoIn ) );

        Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.white , "_Color" ).setEaseType( GoEaseType.ExpoOut ) );
        Go.to( transform.GetChild( 1 ) , 0.4f , new GoTweenConfig().materialColor( Color.white , "_EmissionColor" ).setEaseType( GoEaseType.ExpoIn ) );

        playerControlled = false;
    }

    #endregion

    #region Node type

    private NodeBase c_nodeBase;
    private NodeBase _nodeBase
    {
        get
        {
            if ( !c_nodeBase )
                c_nodeBase = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent< NodeBase >();

            return c_nodeBase;
        }
    }

    public void SetNodeType ( string type )
    {
        nodeType = type;

        switch ( nodeType )
        {
            case "botnet":

                transform.GetChild( 0 ).GetComponent<MeshFilter>().mesh = cube;
                transform.GetChild( 0 ).position = transform.position + Vector3.up;

                break;

            case "home":
                transform.GetChild( 0 ).GetComponent<MeshFilter>().mesh = cube;
                transform.GetChild( 0 ).position = transform.position + ( Vector3.up * 3f );

                break;

            case "firewall":

                transform.GetChild( 0 ).GetComponent<MeshFilter>().mesh = pyramid;
                transform.localRotation = Quaternion.LookRotation( Vector3.forward );
                transform.GetChild( 0 ).position = transform.position + ( Vector3.back * 0.25f ) + Vector3.up;

                capacity = 10f;

                break;

            case "router":

                transform.GetChild( 0 ).GetComponent<MeshFilter>().mesh = sphere;
                transform.GetChild( 0 ).position = transform.position + Vector3.up;

                capacity = 10f;

                break;

            case "blackice" :

                transform.GetChild( 0 ).GetComponent<MeshFilter>().mesh = pyramid;

                transform.localRotation = Quaternion.LookRotation( Vector3.forward );
                transform.GetChild( 0 ).position = transform.position + ( Vector3.back * 0.42f ) + Vector3.up;

                capacity = 20f;
                WatchPackets();

                break;

            case "target":

                transform.GetChild( 0 ).GetComponent<MeshFilter>().mesh = sphere;

                transform.localScale = Vector3.zero;
                Go.to( transform , 0.8f , new GoTweenConfig().scale( new Vector3( 4f , 4f , 4f ) ).setEaseType( GoEaseType.ExpoIn ).onComplete( c =>
                    Go.to( transform , 1.6f , new GoTweenConfig().scale( new Vector3( 2f , 2f , 2f ) ).setEaseType( GoEaseType.BounceOut ) )
                    ) );

                capacity = 1f;
                WatchPackets();

                break;
        }
    }

    public void SelectNode()
    {
        if ( playerControlled )
        {
            Go.killAllTweensWithTarget( transform.GetChild( 0 ) );
            Go.to( transform.GetChild( 0 ) , 0.2f , new GoTweenConfig().scale( 1f ).setEaseType( GoEaseType.BackOut ) );

            foreach ( Node node in connections )
            {
                if ( !node.playerControlled )
                    node.ShowConnectionHighlight();
            }
        }
    }

    public void DeselectNode()
    {
        if ( playerControlled )
        {
            Go.killAllTweensWithTarget( transform.GetChild( 0 ) );
            Go.to( transform.GetChild( 0 ) , 0.8f , new GoTweenConfig().scale( 0.5f ).setEaseType( GoEaseType.BounceOut) );

            foreach ( Node node in connections )
            {
                node.HideConnectionHighlight();
            }
        }
    }

    #endregion

    #region Variables

    private float capacity = 5f;
    private float packets = 0;
    private float load = 0;
    private bool updateLoad = false;

    [HideInInspector] public string nodeType;
    [HideInInspector] public float linksInProgress = 0f;

    [HideInInspector] public List< Node > connections = new List<Node>();
    [HideInInspector] public List< Node > links = new List<Node>();

    public GameObject nodeConnectionPrefab;
    public GameObject nodeLinkPrefab;
    public GameObject backTraceLinkPrefab;
    public Mesh cube;
    public Mesh sphere;
    public Mesh pyramid;

    [HideInInspector] public bool playerControlled;

    public float sendPacketInterval = 0.5f;
    public GameObject packetPrefab;
    private int packetTarget = 0;
    private Vector3[] bezierCircle = new Vector3[ 16 ];

    private LineRenderer c_lineRenderer;
    private LineRenderer _lineRenderer
    {
        get
        {
            if ( !c_lineRenderer )
                c_lineRenderer = GetComponent<LineRenderer>();

            return c_lineRenderer;
        }
    }

    #endregion
}
